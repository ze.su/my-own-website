/*
Navicat MySQL Data Transfer

Source Server         : line
Source Server Version : 50148
Source Host           : qdm125056185.my3w.com:3306
Source Database       : qdm125056185_db

Target Server Type    : MYSQL
Target Server Version : 50148
File Encoding         : 65001

Date: 2015-12-10 15:36:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_admin
-- ----------------------------
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE `t_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员账号编号',
  `admin_name` varchar(255) DEFAULT NULL COMMENT '管理员账户',
  `admin_password` varchar(255) DEFAULT NULL COMMENT '管理员密码',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_admin
-- ----------------------------
INSERT INTO `t_admin` VALUES ('1', '86454224@qq.com', '15453e92cc3fc56506c3564ba3d1e9b6');
INSERT INTO `t_admin` VALUES ('2', 'admin', 'nimda');

-- ----------------------------
-- Table structure for t_article_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_article_tag`;
CREATE TABLE `t_article_tag` (
  `article_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`tag_id`),
  KEY `fk_article_tag_tag` (`tag_id`),
  CONSTRAINT `fk_article_tag_article` FOREIGN KEY (`article_id`) REFERENCES `t_articles` (`article_id`),
  CONSTRAINT `fk_article_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `t_tags` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_article_tag
-- ----------------------------
INSERT INTO `t_article_tag` VALUES ('36', '1');
INSERT INTO `t_article_tag` VALUES ('37', '1');
INSERT INTO `t_article_tag` VALUES ('34', '6');
INSERT INTO `t_article_tag` VALUES ('30', '8');
INSERT INTO `t_article_tag` VALUES ('32', '8');
INSERT INTO `t_article_tag` VALUES ('33', '8');

-- ----------------------------
-- Table structure for t_articles
-- ----------------------------
DROP TABLE IF EXISTS `t_articles`;
CREATE TABLE `t_articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章编号',
  `article_title` varchar(255) DEFAULT NULL COMMENT '文章标题',
  `article_content` longtext COMMENT '文章内容',
  `article_date` timestamp NULL DEFAULT NULL COMMENT '文章时间',
  `article_views` int(255) DEFAULT '0' COMMENT '文章点击率',
  `article_zan` int(255) DEFAULT '0' COMMENT '文章点赞数',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_articles
-- ----------------------------
INSERT INTO `t_articles` VALUES ('29', '世界，你好！', '<p>\r\n	终于，\r\n</p>\r\n<p>\r\n	做好了，\r\n</p>\r\n<p>\r\n	与大家见面了。\r\n</p>', '2015-04-10 09:25:10', '208', '8');
INSERT INTO `t_articles` VALUES ('30', '腾讯面试', '<p style=\"text-align:justify;\">\n	<span style=\"font-size:24px;\"><span style=\"font-size:18px;\"><span style=\"font-size:16px;\"> 今天参加了腾讯的面试，发现要解决的东西还真不少啊！自己要学习的太多了。以后要加油了！</span></span></span> \n</p>\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>', '2015-04-15 23:18:25', '244', '7');
INSERT INTO `t_articles` VALUES ('32', '啊啊啊啊 ', '<span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span><span>面试好难</span></span><span></span>', '2015-04-15 23:20:28', '298', '39');
INSERT INTO `t_articles` VALUES ('33', '啊啊啊啊啊啊啊', '啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊                               	                                    ', '2015-05-26 13:51:47', '645', '5');
INSERT INTO `t_articles` VALUES ('34', 'SDd', 'dsafasdf', '2015-08-07 11:58:19', '516', '4');
INSERT INTO `t_articles` VALUES ('35', 'JS', '', '2015-10-22 11:24:07', '535', '0');
INSERT INTO `t_articles` VALUES ('36', 'JS控制停止加载页面', '<p>\n	<span style=\"color:#333333;font-family:Verdana, Geneva, Arial, Helvetica, sans-serif;font-size:12.5px;line-height:22.5px;background-color:#FFFFFF;\">IE浏览器用\n<pre class=\"prettyprint lang-js\">document.execCommand(\"Stop\");</pre>\nChrome和Firefox用\n<pre class=\"prettyprint lang-js\">window.stop();</pre>\n</span>\n</p>\n<p>\n	<span style=\"color:#333333;font-family:Verdana, Geneva, Arial, Helvetica, sans-serif;font-size:12.5px;line-height:22.5px;background-color:#FFFFFF;\">简写</span>\n</p>\n<pre class=\"prettyprint lang-js\">window.stop ? window.stop() : document.execCommand(\"Stop\");</pre>', '2015-10-22 11:25:10', '745', '1');
INSERT INTO `t_articles` VALUES ('37', 'IE9及以上浏览器或者Chrome打开', '<pre class=\"prettyprint lang-js\"> var ua = navigator.userAgent;\n if(ua.indexOf(\"Chrome\") &lt;= 0\n    &amp;&amp; ua.indexOf(\"MSIE 9.0\") &lt;= 0\n    &amp;&amp; ua.indexOf(\"MSIE 10.0\") &lt;= 0\n    &amp;&amp; !(Object.hasOwnProperty.call(window, \"ActiveXObject\")\n    &amp;&amp; !window.ActiveXObject)) {\n     alert(\"请使用Chrome或IE9及以上浏览器打开\");\n     window.stop ? window.stop() : document.execCommand(\"Stop\"); // 停止继续加载页面\n }</pre>', '2015-10-22 14:40:21', '763', '4');

-- ----------------------------
-- Table structure for t_tags
-- ----------------------------
DROP TABLE IF EXISTS `t_tags`;
CREATE TABLE `t_tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标签编号',
  `tag_name` varchar(255) DEFAULT NULL COMMENT '标签名称',
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_tags
-- ----------------------------
INSERT INTO `t_tags` VALUES ('1', 'JavaScript');
INSERT INTO `t_tags` VALUES ('2', 'HTML');
INSERT INTO `t_tags` VALUES ('3', 'CSS');
INSERT INTO `t_tags` VALUES ('4', 'JQuery');
INSERT INTO `t_tags` VALUES ('5', 'HTML5');
INSERT INTO `t_tags` VALUES ('6', 'CSS3');
INSERT INTO `t_tags` VALUES ('7', 'AngularJS');
INSERT INTO `t_tags` VALUES ('8', '面试经验');
INSERT INTO `t_tags` VALUES ('9', 'Ubuntu');
INSERT INTO `t_tags` VALUES ('10', 'NodeJS');
