<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php echo $article -> article_title;?> - SU_ZE__</title>
        <base href="<?php echo site_url();?>" />
        <link rel="shortcut icon" href="assets/front/img/favicon.ico" />
        <link rel="stylesheet" href="assets/front/css/views/articles.css" />
        <script>
            var _hmt = _hmt || [];
            (function() {
                var hm = document.createElement("script");
                hm.src = "//hm.baidu.com/hm.js?24bf248a6fb87aaac6219e68f26e4000";
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(hm, s);
            })();
        </script>
    </head>
    <body>
        <?php include("sidebar.php");?>
        <div class="container" >
            <?php include("header.php");?>
            <p class="zan-add nodisp">赞 + 1</p>
            <div class="article">
                <div class="article-loading">
                    文章加载中...
                </div>
            </div>
        </div>
        <?php include("footer.php");?>
<!--        <script src="assets/front/js/views/articles/entry.js"></script>-->
        <script src="assets/front/js/jquery-1.11.2.min.js"></script>
        <script src="assets/front/js/common.js"></script>
        <script src="assets/front/js/sidebar.js"></script>
        <script src="assets/front/lib/syntaxhighlighter/scripts/shCore.js"></script>
        <script src="assets/front/lib/syntaxhighlighter/scripts/shBrushJScript.js"></script>
        <script src="assets/front/js/common/syntaxhighlighter.js"></script>
        <script>
            $(function(){
                SyntaxHighlighter.all();
                var someHeight = $(".article").css("height");
                $(".rightbar").height((parseInt(someHeight) + 51) + "px");

                // 加载文章

                var article_id = "<?php echo $article -> article_id;?>";
                $.get("welcome/ajax_get_article", {
                    article_id: article_id
                }, function(data) {
                    var articleStr = "";
                    if (data == "fail") {
                        alert("加载失败，请重试！");
                    } else {
                        $(".article-loading").fadeOut();
                        var articleTitle = data.article_title,
                            articleDate = data.article_date,
                            articleViews = data.article_views,
                            articleZan = data.article_zan,
                            articleContent = data.article_content;
                        articleStr +=
                            '<h1 class="article-title">' + articleTitle + '</h1>' +
                            '<p class="article-info">' +
                                '<span class="article-time">' + articleDate + '</span>&nbsp;' +
                                '<span>' + articleViews + '&nbsp;次阅读</span>&nbsp;' +
                                '<span class="zan-num" >' + articleZan + '&nbsp;个赞</span>' +
                            '</p>' +
                            '<div class="article-content">' +
                                articleContent +
                            '</div>' +
                            '<div class="zan">' +
                                '<i class="fa fa-heart fa-3"></i>' +
                            '</div>';
                        $(".article").append(articleStr);
                    }
                }, "json");

                // 点赞
                if ($(".zan")) {
                    $(".zan").on("click", function(){
                        alert(1);
                        zan();
                    });
                }

                function zan() {
                    $.get("welcome/zan", {article_id: "<?php echo $article -> article_id;?>"},function(data){
                        if(data == "success"){
                            $(".zan-num").text("").text("<?php echo $article -> article_zan;?> 个赞");
                            $(".zan-add").fadeIn(1000).fadeOut(1000);
                        }
                    });
                }
            });
        </script>
    </body>
</html>