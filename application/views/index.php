<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SU_ZE__' Blog</title>
        <base href="<?php echo site_url();?>" />
        <link rel="shortcut icon" href="assets/front/img/favicon.ico" />
        <link rel="stylesheet" href="assets/front/css/views/index.css" />
        <script>
            var _hmt = _hmt || [];
            (function() {
                var hm = document.createElement("script");
                hm.src = "//hm.baidu.com/hm.js?24bf248a6fb87aaac6219e68f26e4000";
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(hm, s);
            })();
        </script>

    </head>
    <body>
        <?php include("sidebar.php");?>
        <?php include("header.php");?>
        <div class="container">
            <div class="article-list">
                <ul class="tags">
                    <li class="active"><a href="">ALL</a></li>
                    <?php
                    	foreach($five_tags as $five_tag){
                    ?>
                    	<li><a href="tags/<?php echo $five_tag -> tag_id;?>"><?php echo $five_tag -> tag_name;?></a></li>
                    <?php
						}
                    ?>
                </ul>
                <div class="clearfix"></div>
                <ul class="blog-list">
                    <div class="blog-list-loading">
                        loading...
                    </div>
                </ul>

                <?php
                	if(count($articles) >= 5 ){
                ?>
                    <div class="load-more">
                        <button type="" class="blog-more" >点击查看更多</button>
                    </div>
                <?php
                	} else{
				?>	
					<button type="" class="blog-more" >更多内容敬请期待...</button>
				<?php
                	}
                ?>
            </div>
        </div>
        <?php include("footer.php");?>
        <script src="assets/front/js/jquery-1.11.2.min.js"></script>
        <script src="assets/front/js/common.js"></script>
        <script src="assets/front/js/sidebar.js"></script>
        <script src="assets/front/lib/syntaxhighlighter/scripts/shCore.js"></script>
        <script src="assets/front/lib/syntaxhighlighter/scripts/shBrushJScript.js"></script>
        <script src="assets/front/js/common/syntaxhighlighter.js"></script>
        <script>
            $(function(){
                var someHeight = $(".article-list").css("height");
                $(".rightbar").height((parseInt(someHeight) + 51) + "px");

                // 加载文章
                ajaxGetBlogs();

                // load-more
                var $blogMore = $(".blog-more");
                $blogMore.on("click", function(){
                    ajaxGetBlogs();
                });
                function ajaxGetBlogs() {
                    $.get("welcome/get_more_articles", {}, function(data){
                        var moreArticleStr = "";
                        if (data == "fail") {
                            alert("数据请求失败");
                            location.reload();
                        } else {
                            $(".blog-list-loading").fadeOut();
                            for (var i = 0; i < data.length; i ++) {
                                // 初始化标签的东西
                                var tagStr = '';
                                var tags = data[i].article_tags;
                                for (var j = 0; j < tags.length; j ++) {
                                    tagStr +=
                                        '<a href="tags/' + tags[j].tag_id + '" class="blog-tags" data-id="' + tags[j].article_id + '"  >' +
                                             tags[j].tag_name +
                                        '</a>';
                                }
                                // 图片的事
                                var article_first_img = "assets/front/img/thisIsAImg.jpg";
                                if (data[i].article_first_img) {
                                    article_first_img = data[i].article_first_img;
                                }

                                // 初始化新出来的文章
                                moreArticleStr +=
                                    '<li class="blog-item">' +
                                        '<div class="blog-left">' +
                                            '<div class="blog-top">' +
                                                '<div class="blog-time">XXX 发表于：' + data[i].article_date + '</div>' +
                                            '</div>' +
                                            '<div class="blog-title">' +
                                                '<a href="articles?article_id=' + data[i].article_id + '">' + data[i].article_title + '</a>' +
                                            '</div>' +
                                            '<div class="blog-info">' +
                                                '<div class="blog-views">' + data[i].article_views + '&nbsp;<i class="fa fa-eye"></i></div>' +
                                                '<div class="blog-zan">' + data[i].article_zan + '&nbsp;<i class="fa fa-heart" style="color: #e78170;"></i></div>' +
                                                tagStr +
                                                '<div class="clearfix"></div>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="blog-right">' +
                                            '<div class="blog-thumb-img">' +
                                                '<a href="articles?article_id=' + data[i].article_id + '"><img src="' + article_first_img + '" alt=""></a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</li>';
                            }
                        }
                        $(".blog-list").append(moreArticleStr);
                    }, "json");
                }
            });
        </script>
    </body>
</html>