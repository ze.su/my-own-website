$(function(){
    var someHeight = $(".article").css("height");
    $(".rightbar").height((parseInt(someHeight) + 51) + "px");

    // 点赞
    $(".zan").on("click", function(){
        $.get("welcome/zan", {article_id: "<?php echo $article -> article_id;?>"},function(data){
            if(data == "success"){
                $(".zan-num").text("").text("<?php echo $article -> article_zan;?> 个赞");
                $(".zan-add").fadeIn(1000).fadeOut(1000);
            }
        });
    });
});